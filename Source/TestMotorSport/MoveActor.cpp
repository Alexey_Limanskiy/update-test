// Fill out your copyright notice in the Description page of Project Settings.


#include "MoveActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "DrawDebugHelpers.h"
#include "Engine/Engine.h"
#include "Math/Vector.h"


// Sets default values
AMoveActor::AMoveActor()
{
	// Create root
	RootScene = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = RootScene;
	// Create Static Mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	//Create All Scene Components

	SceneForward = CreateDefaultSubobject <USceneComponent>(TEXT("SceneForward"));
	SceneForward->SetupAttachment(Mesh);

	SceneRight = CreateDefaultSubobject <USceneComponent>(TEXT("SceneRight"));
	SceneRight->SetupAttachment(Mesh);

	SceneLeft = CreateDefaultSubobject <USceneComponent>(TEXT("SceneLeft"));
	SceneLeft->SetupAttachment(Mesh);

	SceneMiddleRight = CreateDefaultSubobject <USceneComponent>(TEXT("SceneMiddleRight"));
	SceneMiddleRight->SetupAttachment(Mesh);

	SceneMiddleLeft = CreateDefaultSubobject <USceneComponent>(TEXT("SceneMiddleLeft"));
	SceneMiddleLeft->SetupAttachment(Mesh);

	
	// Set this actor to call Tick() every frame. 
	//You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PitchValue = 0.f;
	YawValue = 0.f;
	RollValue = 0.f;

	ForwardHand = false;
	RightHand = false;
	LeftHand = false;
	MiddleRight = false;
	MiddleLeft = false;
	EnableTrace = true;

	Distance = 0.f;
	DistanceMiddleRight = 0.f;
	DistanceMiddleLeft = 0.f;

	FactorRotation = -300.f;
	SmallFactorRotation = -100.f;

	LengthForwardTrace = 300.f;

}

// Called when the game starts or when spawned
void AMoveActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMoveActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//CREATE ALL TRACE



	//Create Line TraceForward

	FHitResult HitForward;
	FVector Start = SceneForward->GetComponentLocation();
	FVector ForwardVector = SceneForward->GetForwardVector();
	FVector End(ForwardVector * LengthForwardTrace + Start);
	FCollisionQueryParams CollisionParams;

	if (EnableTrace)
	{
		DrawDebugLine(GetWorld(), Start, End, FColor::Black, false);
	}

	bool IsHitForward = GetWorld()->LineTraceSingleByChannel(HitForward, Start, End, ECC_Visibility, CollisionParams);

	//Create Line TraceRight
	FHitResult HitRight;
	FVector StartTrace = SceneRight->GetComponentLocation();
	FVector ForwardVectorForRight = SceneRight->GetForwardVector();
	FVector EndTrace(ForwardVectorForRight * 150.f + StartTrace);
	FCollisionQueryParams CollisionParamsForRight;

	if (EnableTrace)
	{
		DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor::Black, false);
	}

	bool IsHitRight = GetWorld()->LineTraceSingleByChannel(HitRight, StartTrace, EndTrace, ECC_Visibility,
		CollisionParamsForRight);

	//Create Line TraceLeft
	FHitResult HitLeft;
	FVector StartLeftTrace = SceneLeft->GetComponentLocation();
	FVector ForwardVectorForLeft = SceneLeft->GetForwardVector();
	FVector EndLeftTrace(ForwardVectorForLeft * 150.f + StartTrace);
	FCollisionQueryParams CollisionParamsForLeft;

	if (EnableTrace)
	{
		DrawDebugLine(GetWorld(), StartLeftTrace, EndLeftTrace, FColor::Black, false);
	}

	bool IsHitLeft = GetWorld()->LineTraceSingleByChannel(HitLeft, StartLeftTrace, EndLeftTrace, ECC_Visibility,
		CollisionParamsForLeft);

	//Create Line TraceMiddleRight

	FHitResult HitMiddleRight;
	FVector StartMiddleRight = SceneMiddleRight->GetComponentLocation();
	FVector ForwardVectorMiddleRight = SceneMiddleRight->GetForwardVector();
	FVector EndMiddleRight = (ForwardVectorMiddleRight * 250.f + StartMiddleRight);
	FCollisionQueryParams CollisionParamsForMeddleRight;

	if (EnableTrace)
	{
		DrawDebugLine(GetWorld(), StartMiddleRight, EndMiddleRight, FColor::Black, false);
	}

	bool isHitMiddleRight = GetWorld()->LineTraceSingleByChannel(HitMiddleRight, StartMiddleRight,
		EndMiddleRight, ECC_Visibility,
		CollisionParamsForMeddleRight);

	//Create Line TraceMiddleLeft

	FHitResult HitMiddleLeft;
	FVector StartMiddleLeft = SceneMiddleLeft->GetComponentLocation();
	FVector ForwardVectorMiddleLeft = SceneMiddleLeft->GetForwardVector();
	FVector EndMiddleLeft = (ForwardVectorMiddleLeft * 250.f + StartMiddleLeft);
	FCollisionQueryParams CollisionParamsForMeddleLeft;

	if (EnableTrace)
	{
		DrawDebugLine(GetWorld(), StartMiddleLeft, EndMiddleLeft, FColor::Black, false);
	}



	bool isHitMiddleLeft = GetWorld()->LineTraceSingleByChannel(HitMiddleLeft, StartMiddleLeft,
		EndMiddleLeft, ECC_Visibility,
		CollisionParamsForMeddleLeft);


	////////////////////////////////////////////

	//Recording bool variables to adjust the movement


	//ForwardHand

	if (IsHitForward)
	{
		if (HitForward.bBlockingHit)
		{
			ForwardHand = true;
		}

		Distance = HitForward.Distance;
	}
	else
	{
		ForwardHand = false;
	}


	//RightHand

	if (IsHitRight)
	{
		if (HitRight.bBlockingHit)
		{
			RightHand = true;
		}
	}
	else
	{
		RightHand = false;
	}


	//LeftHand


	if (IsHitLeft)
	{
		if (HitLeft.bBlockingHit)
		{
			LeftHand = true;
		}
	}
	else
	{
		LeftHand = false;
	}

	//MiddleRightHand

	if (isHitMiddleRight)
	{
		if (HitMiddleRight.bBlockingHit)
		{
			DistanceMiddleRight = HitMiddleRight.Distance;
			MiddleRight = true;
		}
	}
	else
	{
		MiddleRight = false;
	}

	//MiddleLeftHand

	if (isHitMiddleLeft)
	{
		if (HitMiddleLeft.bBlockingHit)
		{
			DistanceMiddleLeft = HitMiddleLeft.Distance;
			MiddleLeft = true;
		}
	}
	else
	{
		MiddleLeft = false;
	}


	////////////////////////////////////////////////////

	//LOGIC OF CAR TURNS


	if (ForwardHand)
	{
		if (Distance > 150.f)
		{
			YawValue = YawValue + SmallFactorRotation * DeltaTime;
		}
		else
		{
			YawValue = YawValue + FactorRotation * DeltaTime;
		}
	}


	if (MiddleRight)
	{
		if (DistanceMiddleRight > 150.f)
		{
			YawValue = YawValue + SmallFactorRotation * DeltaTime;
		}
		else
		{
			YawValue = YawValue + FactorRotation * DeltaTime;
		}
	}

	if (MiddleLeft)
	{
		if (DistanceMiddleLeft > 150.f)
		{
			YawValue = ((YawValue + SmallFactorRotation) * -1.f * DeltaTime);
		}
		else
		{
			YawValue = ((YawValue + FactorRotation + 150.f) * -1.f * DeltaTime);
		}
	}

	if (RightHand)
	{
		YawValue = - 100.f * DeltaTime;
	}

	if (LeftHand)
	{
		YawValue = 100.f * DeltaTime;
	}

	if (MiddleLeft)
	{
		ForwardHand = false;
	}


	if ((!ForwardHand) && (!MiddleRight) && (!MiddleLeft) && (!RightHand) && (!LeftHand))
	{
		YawValue = 0.f;
	}


	//////////////////////////////////////////////////

	//Rotation by default

	FQuat QuatRotation = FRotator(PitchValue, YawValue, RollValue).Quaternion();

	AddActorLocalRotation(QuatRotation, false, 0, ETeleportType::None);

	//The car is moving forward

	FVector Location = GetActorLocation();

	Location += GetActorForwardVector() * Speed * DeltaTime;

	SetActorLocation(Location);

}


