// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DynamicObstacle.generated.h"

UCLASS()
class TESTMOTORSPORT_API ADynamicObstacle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADynamicObstacle();

protected:

	

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	// declare our float variables 
	float RunningTime;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float XValue;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float YValue;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float ZValue;



	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
