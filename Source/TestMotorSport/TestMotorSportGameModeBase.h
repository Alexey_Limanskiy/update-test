// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestMotorSportGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTMOTORSPORT_API ATestMotorSportGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
