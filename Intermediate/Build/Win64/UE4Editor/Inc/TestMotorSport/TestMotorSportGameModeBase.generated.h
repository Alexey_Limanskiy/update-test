// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTMOTORSPORT_TestMotorSportGameModeBase_generated_h
#error "TestMotorSportGameModeBase.generated.h already included, missing '#pragma once' in TestMotorSportGameModeBase.h"
#endif
#define TESTMOTORSPORT_TestMotorSportGameModeBase_generated_h

#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_RPC_WRAPPERS
#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATestMotorSportGameModeBase(); \
	friend struct Z_Construct_UClass_ATestMotorSportGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATestMotorSportGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TestMotorSport"), NO_API) \
	DECLARE_SERIALIZER(ATestMotorSportGameModeBase)


#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATestMotorSportGameModeBase(); \
	friend struct Z_Construct_UClass_ATestMotorSportGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATestMotorSportGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TestMotorSport"), NO_API) \
	DECLARE_SERIALIZER(ATestMotorSportGameModeBase)


#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATestMotorSportGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestMotorSportGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestMotorSportGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestMotorSportGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestMotorSportGameModeBase(ATestMotorSportGameModeBase&&); \
	NO_API ATestMotorSportGameModeBase(const ATestMotorSportGameModeBase&); \
public:


#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATestMotorSportGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATestMotorSportGameModeBase(ATestMotorSportGameModeBase&&); \
	NO_API ATestMotorSportGameModeBase(const ATestMotorSportGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATestMotorSportGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATestMotorSportGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATestMotorSportGameModeBase)


#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_12_PROLOG
#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_RPC_WRAPPERS \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_INCLASS \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTMOTORSPORT_API UClass* StaticClass<class ATestMotorSportGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestMotorSport_Source_TestMotorSport_TestMotorSportGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
