// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TESTMOTORSPORT_MoveActor_generated_h
#error "MoveActor.generated.h already included, missing '#pragma once' in MoveActor.h"
#endif
#define TESTMOTORSPORT_MoveActor_generated_h

#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_RPC_WRAPPERS
#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMoveActor(); \
	friend struct Z_Construct_UClass_AMoveActor_Statics; \
public: \
	DECLARE_CLASS(AMoveActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestMotorSport"), NO_API) \
	DECLARE_SERIALIZER(AMoveActor)


#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMoveActor(); \
	friend struct Z_Construct_UClass_AMoveActor_Statics; \
public: \
	DECLARE_CLASS(AMoveActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TestMotorSport"), NO_API) \
	DECLARE_SERIALIZER(AMoveActor)


#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMoveActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMoveActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMoveActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMoveActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMoveActor(AMoveActor&&); \
	NO_API AMoveActor(const AMoveActor&); \
public:


#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMoveActor(AMoveActor&&); \
	NO_API AMoveActor(const AMoveActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMoveActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMoveActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMoveActor)


#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RootScene() { return STRUCT_OFFSET(AMoveActor, RootScene); } \
	FORCEINLINE static uint32 __PPO__Mesh() { return STRUCT_OFFSET(AMoveActor, Mesh); } \
	FORCEINLINE static uint32 __PPO__SceneForward() { return STRUCT_OFFSET(AMoveActor, SceneForward); } \
	FORCEINLINE static uint32 __PPO__SceneRight() { return STRUCT_OFFSET(AMoveActor, SceneRight); } \
	FORCEINLINE static uint32 __PPO__SceneLeft() { return STRUCT_OFFSET(AMoveActor, SceneLeft); } \
	FORCEINLINE static uint32 __PPO__SceneMiddleRight() { return STRUCT_OFFSET(AMoveActor, SceneMiddleRight); } \
	FORCEINLINE static uint32 __PPO__SceneMiddleLeft() { return STRUCT_OFFSET(AMoveActor, SceneMiddleLeft); }


#define TestMotorSport_Source_TestMotorSport_MoveActor_h_9_PROLOG
#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_PRIVATE_PROPERTY_OFFSET \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_RPC_WRAPPERS \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_INCLASS \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TestMotorSport_Source_TestMotorSport_MoveActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_PRIVATE_PROPERTY_OFFSET \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_INCLASS_NO_PURE_DECLS \
	TestMotorSport_Source_TestMotorSport_MoveActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TESTMOTORSPORT_API UClass* StaticClass<class AMoveActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TestMotorSport_Source_TestMotorSport_MoveActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
