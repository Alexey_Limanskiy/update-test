// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestMotorSport/MoveActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoveActor() {}
// Cross Module References
	TESTMOTORSPORT_API UClass* Z_Construct_UClass_AMoveActor_NoRegister();
	TESTMOTORSPORT_API UClass* Z_Construct_UClass_AMoveActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_TestMotorSport();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AMoveActor::StaticRegisterNativesAMoveActor()
	{
	}
	UClass* Z_Construct_UClass_AMoveActor_NoRegister()
	{
		return AMoveActor::StaticClass();
	}
	struct Z_Construct_UClass_AMoveActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnableTrace_MetaData[];
#endif
		static void NewProp_EnableTrace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_EnableTrace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MiddleLeft_MetaData[];
#endif
		static void NewProp_MiddleLeft_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_MiddleLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MiddleRight_MetaData[];
#endif
		static void NewProp_MiddleRight_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_MiddleRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftHand_MetaData[];
#endif
		static void NewProp_LeftHand_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_LeftHand;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightHand_MetaData[];
#endif
		static void NewProp_RightHand_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RightHand;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForwardHand_MetaData[];
#endif
		static void NewProp_ForwardHand_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ForwardHand;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LengthForwardTrace_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LengthForwardTrace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmallFactorRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmallFactorRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FactorRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FactorRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RollValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YawValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_YawValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PitchValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PitchValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceMiddleLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistanceMiddleLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceMiddleRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistanceMiddleRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Distance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Speed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMiddleLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneMiddleLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMiddleRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneMiddleRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneRight_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneRight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneForward_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneForward;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootScene_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootScene;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMoveActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TestMotorSport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoveActor.h" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_EnableTrace_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
		{ "ToolTip", "Enable ore disable line traces" },
	};
#endif
	void Z_Construct_UClass_AMoveActor_Statics::NewProp_EnableTrace_SetBit(void* Obj)
	{
		((AMoveActor*)Obj)->EnableTrace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_EnableTrace = { "EnableTrace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMoveActor), &Z_Construct_UClass_AMoveActor_Statics::NewProp_EnableTrace_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_EnableTrace_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_EnableTrace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleLeft_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	void Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleLeft_SetBit(void* Obj)
	{
		((AMoveActor*)Obj)->MiddleLeft = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleLeft = { "MiddleLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMoveActor), &Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleLeft_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleRight_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	void Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleRight_SetBit(void* Obj)
	{
		((AMoveActor*)Obj)->MiddleRight = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleRight = { "MiddleRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMoveActor), &Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleRight_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_LeftHand_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	void Z_Construct_UClass_AMoveActor_Statics::NewProp_LeftHand_SetBit(void* Obj)
	{
		((AMoveActor*)Obj)->LeftHand = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_LeftHand = { "LeftHand", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMoveActor), &Z_Construct_UClass_AMoveActor_Statics::NewProp_LeftHand_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_LeftHand_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_LeftHand_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_RightHand_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	void Z_Construct_UClass_AMoveActor_Statics::NewProp_RightHand_SetBit(void* Obj)
	{
		((AMoveActor*)Obj)->RightHand = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_RightHand = { "RightHand", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMoveActor), &Z_Construct_UClass_AMoveActor_Statics::NewProp_RightHand_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_RightHand_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_RightHand_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_ForwardHand_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
		{ "ToolTip", "to adjust the movement" },
	};
#endif
	void Z_Construct_UClass_AMoveActor_Statics::NewProp_ForwardHand_SetBit(void* Obj)
	{
		((AMoveActor*)Obj)->ForwardHand = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_ForwardHand = { "ForwardHand", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMoveActor), &Z_Construct_UClass_AMoveActor_Statics::NewProp_ForwardHand_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_ForwardHand_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_ForwardHand_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_LengthForwardTrace_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_LengthForwardTrace = { "LengthForwardTrace", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, LengthForwardTrace), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_LengthForwardTrace_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_LengthForwardTrace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_SmallFactorRotation_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_SmallFactorRotation = { "SmallFactorRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, SmallFactorRotation), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_SmallFactorRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_SmallFactorRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_FactorRotation_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
		{ "ToolTip", "Factor rotation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_FactorRotation = { "FactorRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, FactorRotation), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_FactorRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_FactorRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_RollValue_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_RollValue = { "RollValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, RollValue), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_RollValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_RollValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_YawValue_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_YawValue = { "YawValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, YawValue), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_YawValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_YawValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_PitchValue_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "MoveActor.h" },
		{ "ToolTip", "for rotation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_PitchValue = { "PitchValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, PitchValue), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_PitchValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_PitchValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleLeft_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleLeft = { "DistanceMiddleLeft", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, DistanceMiddleLeft), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleRight_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleRight = { "DistanceMiddleRight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, DistanceMiddleRight), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_Distance_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_Distance = { "Distance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, Distance), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_Distance_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_Distance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_Speed_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "ModuleRelativePath", "MoveActor.h" },
		{ "ToolTip", "declare our float variables" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_Speed = { "Speed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, Speed), METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_Speed_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_Speed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleLeft_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleLeft = { "SceneMiddleLeft", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, SceneMiddleLeft), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleRight_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleRight = { "SceneMiddleRight", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, SceneMiddleRight), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneLeft_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneLeft = { "SceneLeft", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, SceneLeft), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneLeft_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneRight_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneRight = { "SceneRight", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, SceneRight), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneRight_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneRight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneForward_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneForward = { "SceneForward", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, SceneForward), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneForward_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneForward_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_Mesh_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_Mesh = { "Mesh", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, Mesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_Mesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_Mesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMoveActor_Statics::NewProp_RootScene_MetaData[] = {
		{ "Category", "MoveActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "MoveActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMoveActor_Statics::NewProp_RootScene = { "RootScene", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMoveActor, RootScene), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::NewProp_RootScene_MetaData, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::NewProp_RootScene_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMoveActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_EnableTrace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_MiddleRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_LeftHand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_RightHand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_ForwardHand,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_LengthForwardTrace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_SmallFactorRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_FactorRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_RollValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_YawValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_PitchValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_DistanceMiddleRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_Distance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_Speed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneMiddleRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneRight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_SceneForward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_Mesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMoveActor_Statics::NewProp_RootScene,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMoveActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMoveActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMoveActor_Statics::ClassParams = {
		&AMoveActor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMoveActor_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AMoveActor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMoveActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMoveActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMoveActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMoveActor, 227778714);
	template<> TESTMOTORSPORT_API UClass* StaticClass<AMoveActor>()
	{
		return AMoveActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMoveActor(Z_Construct_UClass_AMoveActor, &AMoveActor::StaticClass, TEXT("/Script/TestMotorSport"), TEXT("AMoveActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMoveActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
