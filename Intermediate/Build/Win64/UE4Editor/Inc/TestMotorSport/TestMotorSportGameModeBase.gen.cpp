// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestMotorSport/TestMotorSportGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTestMotorSportGameModeBase() {}
// Cross Module References
	TESTMOTORSPORT_API UClass* Z_Construct_UClass_ATestMotorSportGameModeBase_NoRegister();
	TESTMOTORSPORT_API UClass* Z_Construct_UClass_ATestMotorSportGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_TestMotorSport();
// End Cross Module References
	void ATestMotorSportGameModeBase::StaticRegisterNativesATestMotorSportGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ATestMotorSportGameModeBase_NoRegister()
	{
		return ATestMotorSportGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ATestMotorSportGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATestMotorSportGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TestMotorSport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATestMotorSportGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TestMotorSportGameModeBase.h" },
		{ "ModuleRelativePath", "TestMotorSportGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATestMotorSportGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATestMotorSportGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATestMotorSportGameModeBase_Statics::ClassParams = {
		&ATestMotorSportGameModeBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A8u,
		METADATA_PARAMS(Z_Construct_UClass_ATestMotorSportGameModeBase_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ATestMotorSportGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATestMotorSportGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATestMotorSportGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATestMotorSportGameModeBase, 1971187555);
	template<> TESTMOTORSPORT_API UClass* StaticClass<ATestMotorSportGameModeBase>()
	{
		return ATestMotorSportGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATestMotorSportGameModeBase(Z_Construct_UClass_ATestMotorSportGameModeBase, &ATestMotorSportGameModeBase::StaticClass, TEXT("/Script/TestMotorSport"), TEXT("ATestMotorSportGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATestMotorSportGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
