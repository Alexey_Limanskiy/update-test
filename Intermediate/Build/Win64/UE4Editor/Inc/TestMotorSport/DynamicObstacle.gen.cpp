// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TestMotorSport/DynamicObstacle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDynamicObstacle() {}
// Cross Module References
	TESTMOTORSPORT_API UClass* Z_Construct_UClass_ADynamicObstacle_NoRegister();
	TESTMOTORSPORT_API UClass* Z_Construct_UClass_ADynamicObstacle();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_TestMotorSport();
// End Cross Module References
	void ADynamicObstacle::StaticRegisterNativesADynamicObstacle()
	{
	}
	UClass* Z_Construct_UClass_ADynamicObstacle_NoRegister()
	{
		return ADynamicObstacle::StaticClass();
	}
	struct Z_Construct_UClass_ADynamicObstacle_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ZValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_YValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_XValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_XValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADynamicObstacle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_TestMotorSport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADynamicObstacle_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DynamicObstacle.h" },
		{ "ModuleRelativePath", "DynamicObstacle.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_ZValue_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "DynamicObstacle.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_ZValue = { "ZValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADynamicObstacle, ZValue), METADATA_PARAMS(Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_ZValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_ZValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_YValue_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "DynamicObstacle.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_YValue = { "YValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADynamicObstacle, YValue), METADATA_PARAMS(Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_YValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_YValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_XValue_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "DynamicObstacle.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_XValue = { "XValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADynamicObstacle, XValue), METADATA_PARAMS(Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_XValue_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_XValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADynamicObstacle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_ZValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_YValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADynamicObstacle_Statics::NewProp_XValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADynamicObstacle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADynamicObstacle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADynamicObstacle_Statics::ClassParams = {
		&ADynamicObstacle::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADynamicObstacle_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ADynamicObstacle_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ADynamicObstacle_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADynamicObstacle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADynamicObstacle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADynamicObstacle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADynamicObstacle, 310015042);
	template<> TESTMOTORSPORT_API UClass* StaticClass<ADynamicObstacle>()
	{
		return ADynamicObstacle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADynamicObstacle(Z_Construct_UClass_ADynamicObstacle, &ADynamicObstacle::StaticClass, TEXT("/Script/TestMotorSport"), TEXT("ADynamicObstacle"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADynamicObstacle);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
